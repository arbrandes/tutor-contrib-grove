import functools
import itertools
from importlib import resources

from tutor import env, hooks, config as tutor_config
from tutormfe.hooks import MFE_APPS

try:
    from tutormfe.plugin import get_mfes
except ImportError:
    get_mfes = None

from .template_filter import (get_base_tenant_configuration,
                              get_site_configuration, get_tenant_configuration,
                              merge_dict)

try:
    from tutormfe.plugin import get_mfes
except ImportError:
    get_mfes = None

try:
    from tutorforum.hooks import FORUM_ENV
except ImportError:
    FORUM_ENV = None

from . import commands
from .__about__ import __version__

################# Configuration
config = {
    # Add here your new settings
    "defaults": {
        "VERSION": __version__,

        # If you want to run the MFE's behind a CDN
        # This is the origin that your CDN will proxy to.
        "MFE_CDN_ORIGIN": "",

        # Set the static URL for LMS/CMS respectively. Leave empty for default.
        "LMS_STATIC_URL": "",
        "CMS_STATIC_URL": "",

        # Set spec.revisionHistoryLimit for any
        # of the default deployments created by tutor.
        "REVISION_HISTORY_LIMIT": 10,

        # Set to True to host static files on S3
        "ENABLE_S3_STATIC_FILES": False,

        # The bucket name to use for S3 static files
        "S3_STATIC_FILES_BUCKET_NAME": "{{S3_STORAGE_BUCKET}}",

        # Enable/disable the Celery beat pods
        "ENABLE_CELERY_BEAT": False,

        # Enable/disable shared Elasticsearch
        "ENABLE_SHARED_ELASTICSEARCH": False,

        "MAINTENANCE_S3_BUCKET_ROOT_URL": "",
        "SERVER_ERROR_S3_BUCKET_ROOT_URL": "",

        # If the comprehensive theme used by the Grove instance contains a npm brand package,
        # set this variable to the name of the package so that it gets installed in the MFE environment.
        "COMPREHENSIVE_THEME_BRAND_PACKAGE_NAME": "",

        # Kubernetes Pod Disruption Budgets -- if set to -1 (not a string), then PDB is disabled
        # Max unavailable is set to 0 to prevent any downtime during upgrades, however this may
        # cause issues in case of many replicas. If the autoscaling is set to other than the
        # default, then the max unavailable sould be considered to be set to a higher value.
        "CADDY_PDB_MAX_UNAVAILABLE": 0,
        "LMS_PDB_MAX_UNAVAILABLE": 0,
        "CMS_PDB_MAX_UNAVAILABLE": 0,
        "LMS_WORKER_PDB_MAX_UNAVAILABLE": 0,
        "CMS_WORKER_PDB_MAX_UNAVAILABLE": 0,
        "FORUM_PDB_MAX_UNAVAILABLE": 0,
        "MFE_PDB_MAX_UNAVAILABLE": 0,

        "CADDY_VOLUME_SIZE": "",
        "ELASTICSEARCH_VOLUME_SIZE": "",
        "REDIS_VOLUME_SIZE": "",

        # Enable automatic waffle flag creation
        #
        # Example:
        #   WAFFLE_FLAGS: [
        #       {
        #           "name": "course_experience.enable_about_sidebar_html",
        #           "everyone": true,
        #       },
        #   ]
        "WAFFLE_FLAGS": [],

        # Additional settings
        "COMMON_ENV_FEATURES": "",
        "COMMON_SETTINGS": "",

        "XBLOCK_HANDLER_TOKEN_KEYS": [],

        "LMS_ENV": "",
        "LMS_ENV_FEATURES": "",
        "LMS_PRODUCTION_SETTINGS": "",

        "CMS_ENV": "",
        "CMS_ENV_FEATURES": "",
        "CMS_PRODUCTION_SETTINGS": "",

        "MFE_LMS_COMMON_SETTINGS": "",

        # Open edX auth settings
        "OPENEDX_AUTH": "",

        # Enable cron jobs
        #
        # Example:
        #   CRON_JOBS: [
        #       {
        #           "name": "run-aggregator-service",
        #           "schedule": "*/10 * * * *",
        #           "script": "./manage.py cms run_aggregator_service"
        #       },
        #       {
        #           "name": "hello-world",
        #           "schedule": "*/20 * * * *",
        #           "script": 'echo "Hello world"',
        #       }
        #   ]
        "CRON_JOBS": [],
        # Additional domains that you would like to add
        # A list of dictionaries with keys `proxy` and `domain`
        # [{"domain": "example.com", "proxy": "lms:8000" }]
        "ADDITIONAL_DOMAINS": [],
        # Extra origins/hosts which need to be allowed and
        # whitelisted for CORS
        "ALLOW_EXTRA_ORIGINS": [],
        # Configure redirect rules through Caddy
        "REDIRECTS": [],
        # eox-tenant for multi domain support
        "USE_EOX_TENANT": False,
        # MFEs listed here will be disabled
        "DISABLED_MFES": [],
        # MFE list to be added/updated
        "NEW_MFES": {},
        # The common CDN origin for MFE themes when using runtime theming
        "MFE_THEME_CDN_ORIGIN": "",
    },
    # Add here settings that don't have a reasonable default for all users. For
    # instance: passwords, secret keys, etc.
    "unique": {},
    # Danger zone! Add here values to override settings from Tutor core or other plugins.
    "overrides": {
        # The list of indexes is defined in:
        # https://github.com/openedx/course-discovery/blob/master/course_discovery/settings/base.py
        # We need to keep a copy of this list so that we can prefix the index names when using shared ES.
        "DISCOVERY_INDEX_OVERRIDES": {
            "course_discovery.apps.course_metadata.search_indexes.documents.course": "{{ ELASTICSEARCH_INDEX_PREFIX }}course",
            "course_discovery.apps.course_metadata.search_indexes.documents.course_run": "{{ ELASTICSEARCH_INDEX_PREFIX }}course_run",
            "course_discovery.apps.course_metadata.search_indexes.documents.learner_pathway": "{{ ELASTICSEARCH_INDEX_PREFIX }}learner_pathway",
            "course_discovery.apps.course_metadata.search_indexes.documents.person": "{{ ELASTICSEARCH_INDEX_PREFIX }}person",
            "course_discovery.apps.course_metadata.search_indexes.documents.program": "{{ ELASTICSEARCH_INDEX_PREFIX }}program",
        }
    },
    "global_defaults": {
        "ELASTICSEARCH_INDEX_PREFIX": "",
        "ELASTICSEARCH_HTTP_AUTH": "",
        "ELASTICSEARCH_CA_CERT_PEM": ""
    }
}


hooks.Filters.ENV_TEMPLATE_FILTERS.add_items([
    ("get_site_configuration", get_site_configuration),
    ("get_base_tenant_configuration", get_base_tenant_configuration),
    ("get_tenant_configuration", get_tenant_configuration)
])


def is_plugin_loaded(plugin_name: str) -> bool:
    """
    Check if the provided plugin is loaded.
    """

    return plugin_name in hooks.Filters.PLUGINS_LOADED.iterate()


hooks.Filters.ENV_TEMPLATE_VARIABLES.add_item(
    ("is_plugin_loaded", is_plugin_loaded)
)

################# Initialization tasks


def _template_escape(text):
    return text.replace("{", "{% raw %}{{% endraw %}")


_LARGE_TASK_CUT_MARKER = "\n#grove-large-task-break\n"

_TASK_SIZE_LIMIT = 100 * (2 ** 10)


def _split_script(script):
    segment = ""
    for src in map(_template_escape, script.split(_LARGE_TASK_CUT_MARKER)):
        if len(segment) + 1 + len(src) < _TASK_SIZE_LIMIT:
            segment += "\n" + src
        else:
            yield segment
            segment = src
    yield segment


def _large_cli_task(service, template, values):
    # Load config as `tutor.commands.jobs:do_callback()` does.
    import click

    context = click.get_current_context().obj
    config = tutor_config.load(context.root)

    script = env.render_str(config, template).strip()
    scripts = _split_script(script)
    scripts = zip(itertools.repeat(service), scripts)
    return values + list(scripts)


tasks_dir = resources.files("tutorgrove") / "templates" / "grove" / "tasks"

for task_file in sorted(tasks_dir.glob("*/*")):
    service, task = task_file.parts[-2:]
    template = task_file.read_text(encoding='utf-8')
    if task.endswith(".large"):
        # The output of some templates may become too large to be
        # passed to sh, so we render them ourselves and break up the
        # result in multiple scripts.
        #
        # See also `docs/large-tasks.rst`.
        callback = functools.partial(_large_cli_task, service, template)
        hooks.Filters.CLI_DO_INIT_TASKS.add()(callback)
    else:
        hooks.Filters.CLI_DO_INIT_TASKS.add_item((service, template))


################# You don't really have to bother about what's below this line,
################# except maybe for educational purposes :)

# Plugin templates
hooks.Filters.ENV_TEMPLATE_ROOTS.add_item(str(resources.files("tutorgrove") / "templates"))

hooks.Filters.ENV_TEMPLATE_TARGETS.add_items(
    [
        ("grove/build", "plugins"),
        ("grove/apps", "plugins"),
        ("grove/k8s", "plugins"),
    ],
)

# The patches from this plugin should be loaded after all other plugins
# so that the overrides configured for individual Grove instances
# are themselves not overriden by the default patches of other plugins.
# The priority of these patches are therefore set to 100.
for patch_file in (resources.files("tutorgrove") / "patches").glob("*"):
    hooks.Filters.ENV_PATCHES.add_item((
        patch_file.name,
        patch_file.read_text(encoding='utf-8')
    ), priority=100)

# Disable patches in the Dockerfile, because they don't apply cleanly
hooks.Filters.ENV_PATCHES.add_item(
  (
  "openedx-dockerfile-git-patches-default",
  "#"
  )
)


# Load all configuration entries
hooks.Filters.CONFIG_DEFAULTS.add_items(
    [
        (f"GROVE_{key}", value)
        for key, value in config["defaults"].items()
    ]
)

# Defaults for values derived from this config
hooks.Filters.CONFIG_DEFAULTS.add_items(list(config["global_defaults"].items()))

hooks.Filters.CONFIG_UNIQUE.add_items(
    [
        (f"GROVE_{key}", value)
        for key, value in config["unique"].items()
    ]
)

hooks.Filters.CONFIG_OVERRIDES.add_items(list(config["overrides"].items()))

hooks.Filters.CLI_COMMANDS.add_item(commands.maintenance_mode)

if MFE_APPS:
    @hooks.Actions.PROJECT_ROOT_READY.add()
    def _modify_mfes(root: str):
        config = tutor_config.load_minimal(root)
        disabled_mfes = config.get('GROVE_DISABLED_MFES', [])
        new_mfes = config.get('GROVE_NEW_MFES', {})
        get_mfes and get_mfes.cache_clear()

        @MFE_APPS.add()
        def _add_remove_mfe(mfes):
            for key in disabled_mfes:
                if key in mfes:
                    mfes.pop(key)
            mfes = merge_dict(mfes, new_mfes)
            return mfes

if FORUM_ENV:
    @FORUM_ENV.add()
    def _add_forum_env_vars(env_vars):
        env_vars.update(
            {
                "SEARCH_SERVER": "{{ ELASTICSEARCH_SCHEME }}://{{ ELASTICSEARCH_HTTP_AUTH and (ELASTICSEARCH_HTTP_AUTH + '@') }}{{ ELASTICSEARCH_HOST }}:{{ ELASTICSEARCH_PORT }}",
                "ELASTICSEARCH_INDEX_PREFIX": '{{ ELASTICSEARCH_INDEX_PREFIX|default("") }}',
            }
        )
        return env_vars
